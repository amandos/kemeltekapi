﻿<?php
header ('Access-Control-Allow-Origin: *');
Header("Content-Type: text/html;charset=UTF-8");
header ('Access-Control-Allow-Headers: Content-Type, X-Auth-Token, Origin, Authorization');
include 'model.php';
include '../auxiliary_function.php';
class Kemeltek extends model
{
  function __construct() {
      $this->auxiliary = new Auxiliary;
      date_default_timezone_set('Asia/Almaty');
      $this->message = $this->auxiliary->lang_message();
  }
  function sign_up(){
    if(!isset($_POST['name_company']) || $_POST['name_company'] == ''){
      echo json_encode(array('type' => 'error','msg'=>$this->message['ENTER_NAME_COMPANY_MESSAGE']));
      die;
    }else{
      if(!$this->auxiliary->check_length_input_var($_POST['name_company'],18,3)){
        echo json_encode(array('type' => 'error','msg'=>$this->message['ENTER_NAME_COMPANY_WORDS_MESSAGE']));
        die;
      }
    }
    if(!isset($_POST['login']) || $_POST['login'] == ''){
      echo json_encode(array('type' => 'error','msg'=>$this->message['ENTER_LOGIN_MESSAGE']));
      die;
    }else{
      if(!$this->auxiliary->check_length_input_var($_POST['login'],18,3)){
        echo json_encode(array('type' => 'error','msg'=>$this->message['ENTER_NAME_COMPANY_WORDS_MESSAGE']));
        die;
      }
    }
    if(!isset($_POST['name_user']) || $_POST['name_user'] == ''){
      echo json_encode(array('type' => 'error','msg'=>$this->message['ENTER_NAME_MESSAGE']));
      die;
    }else{
      if(!$this->auxiliary->check_length_input_var($_POST['name_user'],18,2)){
        echo json_encode(array('type' => 'error','msg'=>$this->message['ENTER_NAME_WORDS_MESSAGE']));
        die;
      }
    }
    if(!isset($_POST['user_email']) || $_POST['user_email'] == ''){
      echo json_encode(array('type' => 'error','msg'=>$this->message['ENTER_EMAIL_MESSAGE']));
      die;
    }else{
      if(!$this->auxiliary->check_length_input_var($_POST['user_email'],32,5)){
        echo json_encode(array('type' => 'error','msg'=>$this->message['ENTER_EMAIL_WORDS_MESSAGE']));
        die;
      }
    }
    if(!isset($_POST['user_phone']) || $_POST['user_phone'] == ''){
      echo json_encode(array('type' => 'error','msg'=>$this->message['ENTER_PHONE_MESSAGE']));
      die;
    }else{
      if(!$this->auxiliary->check_length_input_var($_POST['user_phone'],18,11)){
        echo json_encode(array('type' => 'error','msg'=>$this->message['ENTER_PHONE_WORDS_MESSAGE']));
        die;
      }
    }
    if(!preg_match_all('/^[\p{Latin}[A-Za-z0-9]+$/m', $_POST['login'])){
      echo json_encode(array('type' => 'error','msg'=>$this->message['ENTER_LOGIN_LATIN_MESSAGE']));
      die;
    }
    $data = array(
        'login'=>$this->auxiliary->trimPostData($_POST['login']),
        'name_company'=>$this->auxiliary->trimPostData($_POST['name_company']),
        'name_user'=>$this->auxiliary->trimPostData($_POST['name_user']),
        'user_email'=>$this->auxiliary->trimPostData($_POST['user_email']),
        'user_phone'=>$this->auxiliary->trimPostData($_POST['user_phone']),
    );
    $check_login = model::check_login_model($data['login']);
    $check_email = model::check_email_model($data['user_email']);
    if($check_login){
      echo json_encode(array('type'=>'error','msg'=>$this->message['LOGIN_ALREADY_MESSAGE']));
      die;
    }elseif ($check_email) {
      echo json_encode(array('type'=>'error','msg'=>$this->message['EMAIL_ALREADY_MESSAGE']));
      die;
    }else{
      $sign_up = model::sign_up_model($data);
      if($sign_up){
        $data_activate = array(
          'user_id'=>$sign_up,
          'code'=>$this->auxiliary->fish_password($this->auxiliary->generate_password(6)),
          'date'=>date("Y-m-d H:i:s"),
          'status'=>0
        );
        $user_activate = model::user_activate_ins($data_activate);
        if($user_activate){
          $letter_dispatch = $this->auxiliary->sending_email($data['user_email'],
          $this->message['SEND_ACTIVATE_SIGNUP_HEADER_LETTER'],
            '<p style="font-size: 14px">'.$this->message['SEND_ACTIVATE_SIGNUP_HELLO_LETTER'].' '.$data['name_user'].',<br>
             '.$this->message['SEND_ACTIVATE_SIGNUP_LINK_LETTER'].'<br>
             <a href="http://localhost:3000/welcome/'.$user_activate.'">
              http://localhost:3000/welcome/'.$user_activate.'
             </a></p><br>
             <span style=" font-size: 14px; line-height: 16px;">'.$this->message['SEND_ACTIVATE_SIGNUP_FOOTER_LETTER'].'</span>');
          if($letter_dispatch){
            echo json_encode(array('type' => 'ok','msg'=>$this->message['SEND_LETTER_ACTIVATION_SIGNUP_MESSAGE']));
            die;
          }else{
            echo json_encode(array('type'=>'error','msg'=>$this->message['SEND_LETTER_ACTIVATION_SIGNUP_ERROR_MESSAGE']));
            die;
          }
        }
      }
    }
  }
  function sign_in(){

    if(!isset($_POST['login']) || $_POST['login'] == ''){
      echo json_encode(array('type' => 'error','msg'=>$this->message['ENTER_EMAIL_OR_LOGIN_MESSAGE']));
      die;
    }
    if(!isset($_POST['pass']) || $_POST['pass'] == ''){
      echo json_encode(array('type' => 'error','msg'=>$this->message['ENTER_PASS_MESSAGE']));
      die;
    }
    $remember = 'false';
    if(isset($_POST['remember'])){$remember = $this->auxiliary->trimPostData($_POST['remember']);}
    $data = array(
        'login'=>$this->auxiliary->trimPostData($_POST['login']),
        'pass'=>$this->auxiliary->fish_password($this->auxiliary->trimPostData($_POST['pass'])),
        'remember'=>$remember,
    );
    $check = model::user_login_model($data);
    if($check){
      $token = array();
      $token['company_id'] = $check['id'];
      $token['time_zone'] = $check['time_zone'];
      $token['login'] = $check['login'];
      $token['lang'] = $check['lang'];
      $token['user_email'] = $check['user_email'];
      $token['db_name'] = $check['db_name'];
      $token['db_user'] = $check['db_user'];
      $token['db_host'] = $check['db_host'];
      $token['db_pass'] = $check['db_pass'];
      $jwt = JWT::encode($token, 'kemeltek_kazakh_alshin');
      if($remember!='false'){
        $browser_uniid = '';
        if(isset($_POST['browser_uniid'])){$browser_uniid = $this->auxiliary->trimPostData($_POST['browser_uniid']);}
        $cookie_data = array(
          'browser_uniid'=>$browser_uniid,
          'company'=>$check
        );
        $cookie = $this->insert_cookie($cookie_data);
        if($cookie){
          echo json_encode(array('type' => 'ok','authToken'=>$jwt,'cookie'=>$cookie));
          die;
        }
      }else{
        echo json_encode(array('type' => 'ok','authToken'=>$jwt));
        die;
      }
    }else{
      echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' user login'));
      die;
    }
  }
  function check_activation(){
    if(!isset($_POST['code']) || $_POST['code'] == ''){
      echo json_encode(array('type' => 'error','msg'=>$this->message['ERROR_GET_ACTIVATION_CODE']));
      die;
    }
    $data_check = array(
      'code'=>$this->auxiliary->trimPostData($_POST['code']),
      'status'=>0
    );
    $check_activation = model::check_activation_code($data_check);
    if($check_activation){
      echo json_encode(array('type' => 'ok'));
      die;
    }
  }
  function welcome_settings(){
    if(!isset($_POST['code']) || $_POST['code'] == ''){
      echo json_encode(array('type' => 'error','msg'=>$this->message['ERROR_GET_ACTIVATION_CODE']));
      die;
    }
    if(!isset($_POST['lang_user']) || $_POST['lang_user'] == ''){
      echo json_encode(array('type' => 'error','msg'=>$this->message['SELECT_LANGUAGE_MESSAGE']));
      die;
    }
    if(!isset($_POST['pass']) || $_POST['pass'] == ''){
      echo json_encode(array('type' => 'error','msg'=>$this->message['ENTER_PASS_MESSAGE']));
      die;
    }else{
      if(!$this->auxiliary->check_length_input_var($_POST['pass'],18,6)){
        echo json_encode(array('type' => 'error','msg'=>$this->message['ENTER_PASS_WORDS_MESSAGE']));
        die;
      }
    }
    if(!isset($_POST['country']) || $_POST['country'] == ''){
      echo json_encode(array('type' => 'error','msg'=>$this->message['SELECT_COUNTRY_MESSAGE']));
      die;
    }
    if(!isset($_POST['city']) || $_POST['city'] == ''){
      echo json_encode(array('type' => 'error','msg'=>$this->message['ENTER_CITY_MESSAGE']));
      die;
    }
    if(!isset($_POST['time_zone']) || $_POST['time_zone'] == ''){
      $time_zone = 'Asia/Almaty';
    }else{
      $time_zone = $_POST['time_zone'];
    }
    if(!isset($_POST['currency']) || $_POST['currency'] == ''){
      $currency =  'KZT';
    }else{
      $currency = $_POST['currency'];
    }
    if(!isset($_POST['currency_symbol']) || $_POST['currency_symbol'] == ''){
      $currency_symbol =  '\u20b8';
    }else{
      $currency_symbol = $_POST['currency_symbol'];
    }
    //activation code - ti tekseru
    $data_check = array(
      'code'=>$this->auxiliary->trimPostData($_POST['code']),
      'status'=>0
    );
    $check_activation = model::check_activation_code($data_check);
    if($check_activation){
      //user malimetterin toliktiru lang,pass,country,city,time_zone
      $data_user = array(
        'user_id' => $check_activation['user_id'],
        'lang'=>$_POST['lang_user'],
        'pass'=>$this->auxiliary->fish_password($this->auxiliary->trimPostData($_POST['pass'])),
        'country'=>$_POST['country'],
        'city'=>$_POST['city'],
        'time_zone'=>$time_zone,
      );
      $user_update = model::user_update_model($data_user);
      //user currency tagaindau
      $data_currency = array(
        'user_id' => $check_activation['user_id'],
        'currency'=>$currency ,
        'currency_symbol'=>$currency_symbol
      );
      $user_currency = model::currency_user_ins($data_currency);
      //baza kosu,tablisalar kosu, baza koldanushisin tagaindau
      $data_db = array(
        'user_id' => $check_activation['user_id'],
        'db_name'=>"db_{$check_activation['login']}",
        'db_user'=>"us_{$check_activation['login']}",
        'db_host'=>"localhost",
        'db_pass'=>$this->auxiliary->fish_password($this->auxiliary->generate_password(6))
      );
      $db_create = model::create_db_model($data_db);
      $create_table = model::create_table_model($data_db);
      $user_db = model::db_user_ins($data_db);
      //user activation satti otkenin belgileu, user activate statusin ozgertu
      $data_activate = array(
        'user_id'=>$check_activation['user_id'],
        'status'=>1,
        'date'=>date("Y-m-d H:i:s")
      );
      $user_activate = model::user_activate_model($data_activate);
      //juege kirgizu, token jiberu - user akparat jane db akparat
      $token = array();
      $token['company_id'] = $check_activation['user_id'];
      $token['time_zone'] = $_POST['time_zone'];
      $token['login'] = $check_activation['login'];
      $token['lang'] = $_POST['lang_user'];
      $token['user_email'] = $check_activation['user_email'];
      $token['db_name'] = $data_db['db_name'];
      $token['db_user'] = $data_db['db_user'];
      $token['db_host'] = $data_db['db_host'];
      $token['db_pass'] = $data_db['db_pass'];
      $jwt = JWT::encode($token, 'kemeltek_kazakh_alshin');
      echo json_encode(array('type' => 'ok','authToken'=>$jwt));
      die;
      //welcome settings bitti
    }
  }
  function sign_in_cookies(){
    if(!isset($_POST['cookie']) || $_POST['cookie'] == ''){
      echo json_encode(array('type' => 'error','msg'=>$this->message['ERROR_COOKIE_GET_MESSAGE']));
      die;
    }
    if(!isset($_POST['browser_uniid']) || $_POST['browser_uniid'] == ''){
      echo json_encode(array('type' => 'error','msg'=>$this->message['ERROR_BROWSER_UNIID_GET_MESSAGE']));
      die;
    }
    $data = array(
      'cookie'=>$this->auxiliary->trimPostData($_POST['cookie']),
      'browser_uniid'=>$this->auxiliary->trimPostData($_POST['browser_uniid']),
    );
    $check = model::sign_in_cookies_model($data);
    if($check){
      $token = array();
      $token['company_id'] = $check['user_id'];
      $token['time_zone'] = $check['time_zone'];
      $token['login'] = $check['login'];
      $token['lang'] = $check['lang'];
      $token['user_email'] = $check['user_email'];
      $token['db_name'] = $check['db_name'];
      $token['db_user'] = $check['db_user'];
      $token['db_host'] = $check['db_host'];
      $token['db_pass'] = $check['db_pass'];
      $jwt = JWT::encode($token, 'kemeltek_kazakh_alshin');
      echo json_encode(array('type' => 'ok','authToken'=>$jwt));
      die;
    }else{
      echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' sign in cookies'));
      die;
    }
  }
  function insert_cookie($data){
    $cookie_data = array(
      'date'=>date("Y-m-d H:i:s"),
      'user_id'=>$data['company']['id'],
      'browser_uniid'=>$data['browser_uniid'],
      'cookie'=>$this->auxiliary->generate_code($data['company']['name_company'])
    );
    $inst = model::insert_cookie_model($cookie_data);
    if($inst){
      return $inst;
    }else{
      return false;
    }
  }
  function forgot_password(){
    if(!isset($_POST['email']) || $_POST['email'] == ''){
      echo json_encode(array('type' => 'error','msg'=>$this->message['ENTER_EMAIL_MESSAGE']));
      die;
    }
    $check = model::check_activation_email_model($this->auxiliary->trimPostData($_POST['email']));
    if($check){
      $password = $this->auxiliary->generate_code($check['id']);
      $data = array(
        'user_id'=>$check['id'],
        'password'=>$password,
        'date'=>date("Y-m-d H:i:s"),
        'status'=>1,
      );
      $update_status = model::update_status_forgotpass_model($data['user_id']);
      $inst = model::forgot_password_model($data);
      if($update_status && $inst){
        $letter_dispatch = $this->auxiliary->sending_email($check['user_email'],$this->message['SEND_FORGOT_PASSWORD_HEADER_LETTER'],
          '<p style="font-size: 14px">'.$this->message['SEND_ACTIVATE_SIGNUP_HELLO_LETTER'].' '.$check['name_user'].',<br>
           '.$this->message['SEND_FORGOT_PASSWORD_LINK_LETTER'].'<br>
           <a href="http://localhost:3000/guest/forgotpassword/'.$password.'">
            http://localhost:3000/guest/forgotpassword/'.$password.'
           </a></p><br>
           <span style=" font-size: 14px; line-height: 16px;">'.$this->message['SEND_ACTIVATE_SIGNUP_FOOTER_LETTER'].'</span>');
        if($letter_dispatch){
          echo json_encode(array('type' => 'ok','msg'=>$this->message['SEND_EMAIL_INSTRUCTIONS_FORGOT_PASS_MESSAGE']));
          die;
        }else{
          echo json_encode(array('type' => 'error','msg'=> ''.$this->message['SYSTEM_ERROR_MESSAGE'].' letter dispatch'));
          die;
        }
      }
    }else{
      echo json_encode(array('type'=>'error','msg'=>$this->message['USER_EMAIL_NOT_REGISTERED_MESSAGE']));
      die;
    }
  }
  function new_password(){
    if(!isset($_POST['code']) || $_POST['code'] == ''){
      echo json_encode(array('type' => 'error','msg'=>$this->message['ERROR_GET_CONFIRMATION_CODE_MESSAGE']));
      die;
    }
    if(!isset($_POST['password']) || $_POST['password'] == ''){
      echo json_encode(array('type' => 'error','msg'=>$this->message['ENTER_PASS_MESSAGE']));
      die;
    }else{
      if(!$this->auxiliary->check_length_input_var($_POST['password'],18,6)){
        echo json_encode(array('type' => 'error','msg'=>$this->message['ENTER_PASS_WORDS_MESSAGE']));
        die;
      }
    }
    if(!isset($_POST['confirm']) || $_POST['confirm'] == ''){
      echo json_encode(array('type' => 'error','msg'=>$this->message['REPEAT_NEW_PASSWORD_MESSAGE']));
      die;
    }
    if($_POST['password'] != $_POST['confirm']){
      echo json_encode(array('type' => 'error','msg'=>$this->message['ERROR_MATCH_PASSWORD']));
      die;
    }
    $check_data = array(
      'password'=>$this->auxiliary->trimPostData($_POST['code']),
      'date'=>date("d-m-Y")
    );
    $check = model::check_code_forgotpass_model($check_data);
    if($check){
      $data=array(
        'user_id'=>$check['user_id'],
        'password'=>$this->auxiliary->fish_password($this->auxiliary->trimPostData($_POST['password'])),
      );
      $update_pass = model::update_pass_model($data);
      $update_status = model::update_status_forgotpass_model($check['user_id']);
      if($update_pass && $update_status){
        $user_info = model::user_info_model($check['user_id']);
        if($user_info){
          $token = array();
          $token['company_id'] = $user_info['id'];
          $token['time_zone'] = $user_info['time_zone'];
          $token['login'] = $user_info['login'];
          $token['lang'] = $user_info['lang'];
          $token['user_email'] = $user_info['user_email'];
          $token['db_name'] = $user_info['db_name'];
          $token['db_user'] = $user_info['db_user'];
          $token['db_host'] = $user_info['db_host'];
          $token['db_pass'] = $user_info['db_pass'];
          $jwt = JWT::encode($token, 'kemeltek_kazakh_alshin');
          echo json_encode(array('type' => 'ok','authToken'=>$jwt));
          die;
        }
      }
    }
  }
}
$Kemeltek = new Kemeltek;
if(!empty($_POST['method'])){
    $get = $_POST['method'];
    if(method_exists($Kemeltek,$get)){
        $Kemeltek->$get();
    }else{
        echo json_encode(array('type' => 'error','sush'));
        die;
    }
}else{
  echo json_encode(array('type' => 'error','get jok demek zapros bolgan jok'));
  die;      //$_POST['method'] jok demek zapros bolgan jok
}
?>
