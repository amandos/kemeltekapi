<?php
class model
{
  function sign_up_model($data){
    $connect = $this->auxiliary->connect();
    $query = "INSERT INTO `users` (`name_company`,`name_user`,`user_email`,`user_phone`,`login`)
    VALUES ('{$data['name_company']}','{$data['name_user']}','{$data['user_email']}','{$data['user_phone']}','{$data['login']}')";
    $result = mysqli_query($connect,$query);
    if(!$connect->error){
      return $connect->insert_id;
    }else{
      echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' sign up model'));
      die;
    }
  }
  function check_activation_code($data){
    $connect = $this->auxiliary->connect();
    $query="SELECT `ua`.`id`,`ua`.`user_id`,`ua`.`code`,`ua`.`date`,`ua`.`status`,
              `u`.`login`,`u`.`name_company`,`u`.`name_user`,
              `u`.`user_email`,`u`.`time_zone`,`u`.`lang`
            FROM user_activate ua
            LEFT JOIN users u ON  u.id = ua.user_id
            WHERE code = '{$data['code']}' and status = '{$data['status']}'";
    $result = mysqli_query($connect,$query);
    if(!$connect->error){
      $num_rows = mysqli_num_rows($result);
      if($num_rows==0){
        echo json_encode(array('type'=>'error','msg'=>$this->message['ERROR_CODE_NOT_VALID_MESSAGE']));
        die;
      }else{
        $myrows = $this->auxiliary->arrayResult($result);
        return $myrows[0];
      }
    }else{
      echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' check activation code'));
      die;
    }
  }
  function user_activate_ins($data){
    $connect = $this->auxiliary->connect();
    $query = "INSERT INTO `user_activate` (`user_id`,`code`,`date`,`status`)
    VALUES ('{$data['user_id']}','{$data['code']}','{$data['date']}','{$data['status']}')";
    $result = mysqli_query($connect,$query);
    if(!$connect->error){
      return $data['code'];
    }else{
      echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' user activate ins'));
      die;
    }
  }
  function user_update_model($data){
    $connect = $this->auxiliary->connect();
    $query="UPDATE `users` set pass = '{$data['pass']}',lang= '{$data['lang']}',time_zone='{$data['time_zone']}',country='{$data['country']}',city='{$data['city']}' WHERE id ='{$data['user_id']}'";
    $result = mysqli_query($connect,$query);
    if(!$connect->error){
      return true;
    }else{
      echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' user update activation'));
      die;
    }
  }
  function currency_user_ins($data){
    $connect = $this->auxiliary->connect();
    $query = "INSERT INTO `user_currency` (`user_id`,`currency`,`currency_symbol`)
      VALUES ('{$data['user_id']}','{$data['currency']}','{$data['currency_symbol']}')";
    $result = mysqli_query($connect,$query);
    if(!$connect->error){
      return true;
    }else{
      echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' currency user ins'));
      die;
    }
  }
  function create_db_model($data){
    $connect = $this->auxiliary->connect();
    $query = "CREATE DATABASE `".$data['db_name']."`";
    mysqli_query($connect,$query);
    if(!$connect->error){
      $query = "CREATE USER `".$data['db_user']."`@`localhost` IDENTIFIED BY '".$data['db_pass']."'";
      mysqli_query($connect,$query);
      if(!$connect->error){
        $query = "GRANT ALL PRIVILEGES ON `".$data['db_name']."`.* TO `".$data['db_user']."`@`localhost`";
        mysqli_query($connect,$query);
        if(!$connect->error){
          return true;
        }else{
          echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' create db user PRIVILEGES model'));
          die;
        }
      }else{
        echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' create db user model'));
        die;
      }
    }else{
      echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' create db model'));
      die;
    }
  }
  function create_table_model($data){
    $token = (object) $data;
    $connect = $this->auxiliary->connect_user($token);
    $querys = array(
      0 => '
        /*goods*/
        CREATE TABLE goods (
          id int(11) NOT NULL AUTO_INCREMENT,
          name varchar(45) DEFAULT NULL,
          category_id int(11) NOT NULL DEFAULT 0,
          color varchar(45) DEFAULT NULL,
          measure int(11) DEFAULT 0,
          image varchar(115) DEFAULT NULL,
          mod_id int(11) DEFAULT NULL,
          barcode varchar(45) DEFAULT NULL,
          sku varchar(45) DEFAULT NULL,
          price varchar(45) DEFAULT 0,
          cost_price varchar(45) DEFAULT 0,
          status_show int(11) DEFAULT 1,
          status_delete int(11) DEFAULT 1,
          PRIMARY KEY (id)
        )
        ENGINE = INNODB
        AUTO_INCREMENT = 1
        CHARACTER SET utf8
        COLLATE utf8_general_ci;
      ',
      1 => '
        /*category*/
        CREATE TABLE category (
          id int(11) NOT NULL AUTO_INCREMENT,
          name varchar(45) DEFAULT NULL,
          parent_category int(11) DEFAULT NULL,
          image varchar(115) DEFAULT NULL,
          color varchar(45) DEFAULT NULL,
          status_delete int(11) DEFAULT NULL,
          status_show int(11) DEFAULT NULL,
          PRIMARY KEY (id)
        )
        ENGINE = INNODB
        AUTO_INCREMENT = 1
        CHARACTER SET utf8
        COLLATE utf8_general_ci;
      ',
    );
    foreach ($querys as $value) {
      $result = mysqli_query($connect,$value);
    }
    if(!$connect->error){
      return true;
    }else{
      echo json_encode(array('type'=>'error','msg'=>''.$connect->error.''.$this->message['SYSTEM_ERROR_MESSAGE'].' create db model'));
      die;
    }
  }
  function user_activate_model($data){
    $connect = $this->auxiliary->connect();
    $query="UPDATE `user_activate` set status = '{$data['status']}',date_ac= '{$data['date']}' WHERE user_id ='{$data['user_id']}'";
    $result = mysqli_query($connect,$query);
    if(!$connect->error){
      return true;
    }else{
      echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' user activate model'));
      die;
    }
  }
  function db_user_ins($data){
    $connect = $this->auxiliary->connect();
    $query = "INSERT INTO `user_db` (`user_id`,`db_name`,`db_user`,`db_host`,`db_pass`)
      VALUES ('{$data['user_id']}','{$data['db_name']}','{$data['db_user']}','{$data['db_host']}','{$data['db_pass']}')";
    $result = mysqli_query($connect,$query);
    if(!$connect->error){
      return true;
    }else{
      echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' db user ins'));
      die;
    }
  }
  function check_login_model($login){
    $connect = $this->auxiliary->connect();
    $login = strtolower($login);
    $query = "SELECT login FROM `users` WHERE LOWER(login)='{$login}'";
    $result = mysqli_query($connect,$query);
    if(!$connect->error){
      $num_rows = mysqli_num_rows($result);
      if($num_rows>0){
        $myrows = $this->auxiliary->arrayResult($result);
        return $myrows[0];
      }else{
        return false;
      }
    }else{
        echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' check company'));
        die;
    }
  }
  function user_login_model($data){
    $connect = $this->auxiliary->connect();
    $login = strtolower($data['login']);
    $query="SELECT u.id,u.time_zone,u.lang,u.name_company,u.name_user,u.user_email,u.pass,u.login,
              ua.status,ud.db_name,ud.db_user,ud.db_host,ud.db_pass
              FROM users u
            LEFT JOIN user_activate ua ON u.id = ua.user_id
            LEFT JOIN user_db ud ON u.id = ud.user_id
            WHERE (LOWER(user_email) = '{$login}' OR LOWER(login) = '{$login}') AND ua.status = 1";
    $result = mysqli_query($connect,$query);
    if(!$connect->error){
      $num_rows = mysqli_num_rows($result);
      if($num_rows==0){
        echo json_encode(array('type'=>'error','msg'=>$this->message['ERROR_USER_NOT_SYSTEM']));
        die;
      }else{
        $myrows = $this->auxiliary->arrayResult($result);
        if($myrows[0]['pass']==$data['pass']) {
          return $myrows[0];
        }else{
          echo json_encode(array('type'=>'error','msg'=>$this->message['ERROR_WRONG_PASSWORD']));
          die;
        }
      }
    }else{
      echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' user login model'));
      die;
    }
  }
  function insert_cookie_model($data){
    $connect = $this->auxiliary->connect();
    $query = "INSERT INTO `user_cookie` (`date`,`user_id`,`browser_uniid`,`cookie`)
      VALUES ('{$data['date']}','{$data['user_id']}','{$data['browser_uniid']}','{$data['cookie']}')";
    $result = mysqli_query($connect,$query);
    if(!$connect->error){
      return $data['cookie'];
    }else{
      echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' insert cookie model'));
      die;
    }
  }
  function sign_in_cookies_model($data){
    $connect = $this->auxiliary->connect();
    $query="SELECT uc.id,uc.browser_uniid,uc.cookie,uc.user_id,
              u.name_company,u.time_zone,u.lang,u.name_user,u.user_email,u.login,
              ud.db_name,ud.db_user,ud.db_host,ud.db_pass
              FROM user_cookie uc
            LEFT JOIN users u ON uc.user_id = u.id
            LEFT JOIN user_db ud ON uc.user_id = ud.user_id
            WHERE browser_uniid = '{$data['browser_uniid']}' and cookie = '{$data['cookie']}'";
    $result = mysqli_query($connect,$query);
    if(!$connect->error){
      $num_rows = mysqli_num_rows($result);
      if($num_rows==0){
        echo json_encode(array('type'=>'error','msg'=>$this->message['ERROR_DATA_EMPTY_SERVER']));
        die;
      }else{
        $myrows = $this->auxiliary->arrayResult($result);
        return $myrows[0];
      }
    }else{
      echo json_encode(array('type'=>'error','msg'=> ''.$this->message['SYSTEM_ERROR_MESSAGE'].' user login model'));
      die;
    }
  }
  function check_code_forgotpass_model($data){
    $connect = $this->auxiliary->connect();
    $query="SELECT id,user_id,password,DATE_FORMAT(`date`, '%d-%m-%Y') as date_forgot,status FROM forgot_password WHERE status = 1 and password = '{$data['password']}' and DATE_FORMAT(`date`, '%d-%m-%Y') = '{$data['date']}'";
    $result = mysqli_query($connect,$query);
    if(!$connect->error){
      $num_rows = mysqli_num_rows($result);
      if($num_rows==0){
        echo json_encode(array('type'=>'error','msg'=>$this->message['ERROR_CODE_NOT_VALID_MESSAGE']));
        die;
      }else{
        $myrows = $this->auxiliary->arrayResult($result);
        return $myrows[0];
      }
    }else{
      echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' check code forgotpass model'));
      die;
    }
  }
  function update_status_forgotpass_model($user_id){
    $connect = $this->auxiliary->connect();
    $query="UPDATE forgot_password set status = 0 WHERE user_id ='{$user_id}'";
    $result = mysqli_query($connect,$query);
    if(!$connect->error){
      return true;
    }else{
      echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' update state forgotpass model'));
      die;
    }
  }
  function user_info_model($user_id){
    $connect = $this->auxiliary->connect();
    $query = "SELECT u.id,u.name_company,u.time_zone,u.lang,u.name_user,u.user_email,u.user_phone,
                u.login,ud.db_name,ud.db_user,ud.db_host,ud.db_pass,ucu.currency,ucu.currency_symbol
                FROM users u
              LEFT JOIN user_db ud ON u.id = ud.user_id
              LEFT JOIN user_currency ucu ON  u.id = ucu.user_id
              WHERE u.id='{$user_id}'";
    $result = mysqli_query($connect,$query);
    if(!$connect->error){
        $myrows = $this->auxiliary->arrayResult($result);
        return $myrows[0];
    }else{
        echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' check email model'));
        die;
    }
  }
  function update_pass_model($data){
    $connect = $this->auxiliary->connect();
    $query="UPDATE users u set u.pass = '{$data['password']}' WHERE id ='{$data['user_id']}'";
    $result = mysqli_query($connect,$query);
    if(!$connect->error){
      return true;
    }else{
      echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' update pass model'));
      die;
    }
  }
  function check_email_model($data){
    $connect = $this->auxiliary->connect();
    $email = strtolower($data);
    $query = "SELECT id,name_company,name_user,user_email FROM `users` WHERE LOWER(user_email)='{$email}'";
    $result = mysqli_query($connect,$query);
    if(!$connect->error){
      $num_rows = mysqli_num_rows($result);
      if($num_rows>0){
        $myrows = $this->auxiliary->arrayResult($result);
        return $myrows[0];
      }else{
        return false;
      }
    }else{
        echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' check email model'));
        die;
    }
  }
  function check_activation_email_model($data){
    $connect = $this->auxiliary->connect();
    $email = strtolower($data);
    $query = "SELECT u.id,u.name_company,u.name_user,u.user_email,ua.status
                FROM users u
              LEFT JOIN user_activate ua ON u.id = ua.user_id
              WHERE LOWER(user_email)='{$email}' AND ua.status = 1";
    $result = mysqli_query($connect,$query);
    if(!$connect->error){
      $num_rows = mysqli_num_rows($result);
      if($num_rows>0){
        $myrows = $this->auxiliary->arrayResult($result);
        return $myrows[0];
      }else{
        echo false;
      }
    }else{
        echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' check activation email model'));
        die;
    }
  }
  function forgot_password_model($data){
    $connect = $this->auxiliary->connect();
    $query = "INSERT INTO `forgot_password` (`user_id`,`password`,`date`,`status`)
    VALUES ('{$data['user_id']}','{$data['password']}','{$data['date']}','{$data['status']}')";
    $result = mysqli_query($connect,$query);
    if(!$connect->error){
      return true;
    }else{
      echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' forgot password model'));
      die;
    }
  }

}
?>
