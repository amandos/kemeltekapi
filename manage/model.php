<?php
class model{
  function user_info_model($user_id){
    $connect = $this->auxiliary->connect();
    $query = "
        SELECT u.id, u.name_company,
               u.name_user,u.user_email,
               u.user_phone,u.login,
               u.time_zone,u.country,
               u.lang,u.city,
               ucu.currency,ucu.currency_symbol
        FROM users u
      LEFT JOIN user_currency ucu ON  u.id = ucu.user_id
      WHERE u.id = '{$user_id}'; ";
    $result = mysqli_query($connect,$query);
    if(!$connect->error){
      $num_rows = mysqli_num_rows($result);
      if($num_rows==0){
        return false;
      }else{
        $myrows = $this->auxiliary->arrayResult($result);
        return $myrows[0];
      }
    }else{
      return false;
    }
  }
  function remove_cookie_model($data){
    $connect = $this->auxiliary->connect();
    $query="DELETE FROM user_cookie WHERE browser_uniid = '{$data['browser_uniid']}' AND user_id ='{$data['user_id']}'";
    $result = mysqli_query($connect,$query);
    if(!$connect->error){
      return true;
    }else{
      echo json_encode(array('type'=>'error','msg'=>''.$connect->error.' remove cookie model'));
      die;
    }
  }
}
?>
