<?php
  header ('Access-Control-Allow-Origin: *');
  header ('Access-Control-Allow-Headers: Content-Type, X-Auth-Token, Origin, Authorization');
  include 'model.php';
  include '../auxiliary_function.php';
  class Manage extends model{
    function __construct() {
      $this->auxiliary = new Auxiliary;
      $this->auxiliary->check_session();
      $this->token = JWT::decode($_POST['token'], 'kemeltek_kazakh_alshin');
      date_default_timezone_set($this->token->time_zone);
      $this->message = $this->auxiliary->lang_message();
    }
    function user_info(){
      $user = model::user_info_model($this->token->company_id);
      if($user){
        $user_app = array(
          'name_company' =>$user['name_company'],
          'name_user' =>$user['name_user'],
          'user_email'=>$user['user_email'],
          'time_zone' =>$user['time_zone'],
          'lang' =>$user['lang'],
          'currency' =>$user['currency'],
          'currency_symbol' =>$user['currency_symbol'],
        );
        echo json_encode(array('type' => 'ok','user_app'=>$user_app));
        die;
      }else{
        echo json_encode(array('type' => 'error','msg'=>$this->message['ERROR_USER_NOT_SYSTEM']));
        die;
      }

    }
    function remove_cookie(){
      $this->auxiliary->check_session();
      if(!isset($_POST['browser_uniid']) || $_POST['browser_uniid'] == ''){
        echo json_encode(array('type' => 'error','msg'=>$this->message['ERROR_BROWSER_UNIID_GET_MESSAGE']));
        die;
      }
      $token = JWT::decode($_POST['token'], 'kemeltek_kazakh_alshin');
      $data = array(
        'browser_uniid'=>$_POST['browser_uniid'],
        'user_id'=>$token->company_id
      );
      $remove = model::remove_cookie_model($data);
      if($remove){
        echo json_encode(array('type' => 'ok'));
        die;
      }
    }
  }
  $Manage = new Manage;
  if(!empty($_POST['method'])){
      $get = $_POST['method'];
      if(method_exists($Manage,$get)){
          $Manage->$get();
      }else{
          echo json_encode(array('type' => 'error','sush'));
          die;
      }
  }else{
    echo json_encode(array('type' => 'error','get jok demek zapros bolgan jok'));
    die;      //$_POST['method'] jok demek zapros bolgan jok
  }
?>
