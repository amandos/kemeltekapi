<?php
  class model{
    /*category*/
    function check_category($data){
      $connect = $this->auxiliary->connect_user($this->token);
      $query="SELECT * FROM category c WHERE c.name='{$data['name_cat']}' AND c.status_delete=1 AND c.parent_category='{$data['parent_cat']}'";
      $result = mysqli_query($connect,$query);
      if(!$connect->error){
        $num_rows = mysqli_num_rows($result);
        if($num_rows>0){
          return false;
        }else{
          return true;
        }
      }else{
          echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' check category model'));
          die;
      }
    }
    function category_add_model($data){
      $connect = $this->auxiliary->connect_user($this->token);
      $query = "INSERT INTO `category` (`name`,`parent_category`,`image`,`color`,`status_delete`,`status_show`)
      VALUES ('{$data['name_cat']}','{$data['parent_cat']}','{$data['image']}','{$data['color_cat']}','{$data['status_delete']}','{$data['status_show']}')";
      $result = mysqli_query($connect,$query);
      if(!$connect->error){
        return true;
      }else{
        echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' category add model'));
        die;
      }
    }

    function category_update_model($data){
      $connect = $this->auxiliary->connect_user($this->token);
      if($data['image'] != 'false'){
        $query="UPDATE `category` set
                  `name` = '{$data['name_cat']}',
                  `parent_category` = '{$data['parent_cat']}',
                  `color` = '{$data['color_cat']}',
                  `image` = '{$data['image']}'
                WHERE id ={$data['cat_id']}";
      }else{
        $query="UPDATE `category` set
                  `name` = '{$data['name_cat']}',
                  `parent_category` = '{$data['parent_cat']}',
                  `color` = '{$data['color_cat']}'
                  WHERE id ={$data['cat_id']}";
      }
      $result = mysqli_query($connect,$query);
      if(!$connect->error){
        return true;
      }else{
        echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' category update model'));
        die;
      }
    }
    function category_info_model($data){
      $connect = $this->auxiliary->connect_user($this->token);
      $query = "SELECT
                  c.id,
                  c.name,
                  c.parent_category,
                  c.image,
                  c.color,
                  cp.name as `parent_category_value`,
                  cp.id as `parent_category_id`,
                  c.status_delete,
                  c.status_show FROM category c
                  LEFT JOIN category cp ON  c.parent_category = cp.id
                  WHERE c.status_delete=1 AND c.id = '{$data['id']}';
               ";
      $result = mysqli_query($connect,$query);
      if(!$connect->error){
          $myrows = $this->auxiliary->arrayResult($result);
          return $myrows;
      }else{
        echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' category info model'));
        die;
      }
    }
    function category_list_filter_model($data){
      $connect = $this->auxiliary->connect_user($this->token);
      $query = "SELECT c.id, c.id as `key`, c.name as `value`,  c.name as `label`, c.name,c.parent_category,c.image,c.color, c.status_delete,c.status_show FROM category c
                  WHERE c.status_delete=1 AND c.id <> '{$data['id']}' AND c.parent_category <> '{$data['id']}';
               ";
      $result = mysqli_query($connect,$query);
      if(!$connect->error){
          $num_rows = mysqli_num_rows($result);
          if($num_rows==0){
            return false;
          }else{
            $myrows = $this->auxiliary->arrayResult($result);
            return $myrows;
          }

      }else{
        echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' category list filter model'));
        die;
      }
    }

    function category_list_model(){
      $connect = $this->auxiliary->connect_user($this->token);
      $query = "SELECT c.id, c.id as `key`, c.name as `value`,  c.name as `label`, c.name,c.parent_category,c.image,c.color, c.status_delete,c.status_show FROM category c
                  WHERE c.status_delete=1;
               ";
      $result = mysqli_query($connect,$query);
      if(!$connect->error){
          $myrows = $this->auxiliary->arrayResult($result);
          return $myrows;
      }else{
        echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' category list model'));
        die;
      }
    }

    function category_status_show_model($data){
      $connect = $this->auxiliary->connect_user($this->token);
      $query="UPDATE `category` set `status_show` = '{$data['status']}' WHERE id ='{$data['cat_id']}'";
      $result = mysqli_query($connect,$query);
      if(!$connect->error){
        return true;
      }else{
        echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' category status show model'));
        die;
      }
    }
    function category_status_delete_model($data){
      $connect = $this->auxiliary->connect_user($this->token);
      $query="UPDATE `category` set `status_delete` = 0 WHERE id IN ({$data['cats']})";
      $result = mysqli_query($connect,$query);
      if(!$connect->error){
        return true;
      }else{
        echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' category status delete model'));
        die;
      }
    }

    /*goods*/
    /*goods add*/
    function delete_in_error_good_mod($good_id){
      $connect = $this->auxiliary->connect_user($this->token);
      $query="DELETE FROM `goods` WHERE id ='{$good_id}' OR mod_id = '{$good_id}'";
      $result = mysqli_query($connect,$query);
      if(!$connect->error){
        return true;
      }else{
        echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' delete in error good mod'));
        die;
      }
    }
    function good_mod_insert_model($data){
      $connect = $this->auxiliary->connect_user($this->token);
      $stmt = $connect->prepare("INSERT INTO `goods` (
      `name`,
      `category_id`,
      `measure`,
      `color`,
      `mod_id`,
      `image`,
      `barcode`,
      `sku`)
      VALUES  (?,?,?,?,?,?,?,?)");
      $stmt->bind_param('ssssssss',
        $data['name'],
        $data['category'],
        $data['measure'],
        $data['color'],
        $data['mod_id'],
        $data['image_goods'],
        $data['barcode'],
        $data['sku']
      );
      $stmt->execute();
      if(!$connect->error){
        return $connect->insert_id;
      }else{
        if($connect->errno == 1062){
          $myrows = explode(" ",$connect->error);
          switch ($myrows[count($myrows)-1]) {
              case "'barcode'":
                echo json_encode(array('type'=>'error','msg'=>$this->message['ERROR_REPEAT_BARCODE_GOODS_MESSAGE']));
                die;
              case "'sku'":
                echo json_encode(array('type'=>'error','msg'=>$this->message['ERROR_REPEAT_SKU_GOODS_MESSAGE']));
                die;
              case "'name'":
                echo json_encode(array('type'=>'error','msg'=>$this->message['ERROR_REPEAT_NAME_GOODS_MESSAGE']));
                die;
              default:
                echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' good mod insert model'));
                die;
          }
        }else{
          echo json_encode(array('type'=>'error','msg'=>$connect->error.''.$this->message['SYSTEM_ERROR_MESSAGE'].' good mod insert model'));
          die;
        }

      }
    }
    function mods_insert_model($data){
      $connect = $this->auxiliary->connect_user($this->token);
      $stmt = $connect->prepare("INSERT INTO `goods` (
        `name`,
        `category_id`,
        `barcode`,
        `sku`,
        `cost_price`,
        `price`,
        `mod_id`)
      VALUES  (?,?,?,?,?,?,?)");
      $stmt->bind_param('sssssss',
        $data['name'],
        $data['category'],
        $data['barcode'],
        $data['sku'],
        $data['cost_price'],
        $data['price'],
        $data['mod_id']
      );
      $stmt->execute();
      if(!$connect->error){
        return true;
      }else{
        $delete_goods = $this->delete_in_error_good_mod($data['mod_id']);
        if($delete_goods){
          if($connect->errno == 1062){
          $myrows = explode(" ",$connect->error);
          switch ($myrows[count($myrows)-1]) {
              case "'barcode'":
                echo json_encode(array('type'=>'error','msg'=>$this->message['ERROR_REPEAT_BARCODE_GOODS_MESSAGE']));
                die;
              case "'sku'":
                echo json_encode(array('type'=>'error','msg'=>$this->message['ERROR_REPEAT_SKU_GOODS_MESSAGE']));
                die;
              case "'name'":
                echo json_encode(array('type'=>'error','msg'=>$this->message['ERROR_REPEAT_NAME_GOODS_MESSAGE']));
                die;
              default:
                echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' mods insert model'));
                die;
          }
        }else{
            echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' mods insert model'));
            die;
        }
        }else{
          echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].''));
          die;
        }
      }
    }
    /*good mod type 0 add*/
    function good_insert_model($data){
      $connect = $this->auxiliary->connect_user($this->token);
      $stmt = $connect->prepare("INSERT INTO `goods` (`name`,`category_id`,`measure`,`color`,`mod_id`,`image`,`cost_price`,`price`,`barcode`,`sku`)
      VALUES  (?,?,?,?,?,?,?,?,?,?)");
      $stmt->bind_param('ssssssssss',
        $data['name'],
        $data['category'],
        $data['measure'],
        $data['color'],
        $data['mod_id'],
        $data['image_goods'],
        $data['cost_price'],
        $data['price'],
        $data['barcode'],
        $data['sku']
      );
      $stmt->execute();
      if(!$connect->error){
        return true;
      }else{
        if($connect->errno == 1062){
          $myrows = explode(" ",$connect->error);
          switch ($myrows[count($myrows)-1]) {
              case "'barcode'":
                echo json_encode(array('type'=>'error','msg'=>$this->message['ERROR_REPEAT_BARCODE_GOODS_MESSAGE']));
                die;
              case "'sku'":
                echo json_encode(array('type'=>'error','msg'=>$this->message['ERROR_REPEAT_SKU_GOODS_MESSAGE']));
                die;
              case "'name'":
                echo json_encode(array('type'=>'error','msg'=>$this->message['ERROR_REPEAT_NAME_GOODS_MESSAGE']));
                die;
              default:
                echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' goods insert model'));
                die;
          }
        }else{
          echo json_encode(array('type'=>'error','msg'=>$connect->error.''.$this->message['SYSTEM_ERROR_MESSAGE'].' goods insert model'));
          die;
        }
      }
    }
    /*goods list*/
    function goods_list_model(){
      $connect = $this->auxiliary->connect_user($this->token);
      $query = "SELECT  g.id AS `goods_id`,
                        g.id AS `key`,
                        g.name AS `goods_name`,
                        g.color,
                        g.measure,
                        g.image,
                        g.mod_id,
                        g.barcode,
                        g.sku,
                        g.price,
                        g.cost_price,
                        ((g.price - g.cost_price)/g.cost_price)*100 AS `markup`,
                        g.status_show,
                        c.id AS `cat_id`,
                        c.name AS `cat_name`
                FROM goods g
                LEFT JOIN category c ON c.id = g.category_id
                WHERE (c.status_show = 1 AND c.status_delete=1 AND g.status_delete=1) OR (c.status_show IS NULL AND c.status_delete IS NULL AND g.status_delete=1)";
      $result = mysqli_query($connect,$query);
      if(!$connect->error){
        $myrows = $this->auxiliary->arrayResult($result);
        return $myrows;
      }else{
        echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' goods list model'));
        die;
      }
    }
    /*good info*/
    function good_info_model($data){
      $connect = $this->auxiliary->connect_user($this->token);
      $query = "SELECT  g.id AS `goods_id`,
                        g.id AS `key`,
                        g.name AS `goods_name`,
                        g.color,
                        g.measure,
                        g.image,
                        g.mod_id,
                        g.barcode,
                        g.sku,
                        g.price,
                        g.cost_price,
                        g.status_show,
                        c.id AS `cat_id`,
                        c.name AS `cat_name`
                FROM goods g
                LEFT JOIN category c ON c.id = g.category_id
                WHERE (g.id = '{$data['id']}' OR g.mod_id = '{$data['id']}') AND c.status_show = 1 AND c.status_delete=1 AND g.status_delete=1";
      $result = mysqli_query($connect,$query);
      if(!$connect->error){
        $myrows = $this->auxiliary->arrayResult($result);
        return $myrows;
      }else{
        echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' good info model'));
        die;
      }
    }
    /*goods update status*/
    function goods_status_show_model($data){
      $connect = $this->auxiliary->connect_user($this->token);
      $query="UPDATE `goods` set `status_show` = '{$data['status']}' WHERE id ='{$data['good_id']}' OR mod_id = '{$data['good_id']}'";
      $result = mysqli_query($connect,$query);
      if(!$connect->error){
        return true;
      }else{
        echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' goods status show model'));
        die;
      }
    }
    /*goods delete*/
    function goods_status_delete_model($data){
      $connect = $this->auxiliary->connect_user($this->token);
      $query="UPDATE `goods` set `status_delete` = 0 WHERE id ='{$data['good_id']}' OR mod_id = '{$data['good_id']}'";
      $result = mysqli_query($connect,$query);
      if(!$connect->error){
        return true;
      }else{
        echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' goods status delete model'));
        die;
      }
    }
    /*mod goods update*/
    function good_mod_update_model($data){
      $connect = $this->auxiliary->connect_user($this->token);
      if($data['image_goods'] != 'false'){
        $query="UPDATE `goods` set
                  `name` = '{$data['name']}',
                  `category_id` = '{$data['category']}',
                  `measure` = '{$data['measure']}',
                  `color` = '{$data['color']}',
                  `mod_id` = '{$data['mod_id']}',
                  `image` = '{$data['image_goods']}'
                WHERE id ={$data['goods_id']}";
      }else{
        $query="UPDATE `goods` set
                    `name` = '{$data['name']}',
                    `category_id` = '{$data['category']}',
                    `measure` = '{$data['measure']}',
                    `color` = '{$data['color']}',
                    `mod_id` = '{$data['mod_id']}'
                  WHERE id ={$data['goods_id']}";
      }
      $result = mysqli_query($connect,$query);
      if(!$connect->error){
        return true;
      }else{
        echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' good mod update model'));
        die;
      }
    }
    /*mods update*/
    function mods_update_model($data){
      $connect = $this->auxiliary->connect_user($this->token);
      $query="UPDATE `goods` set
                `name` = '{$data['name']}',
                `category_id` = '{$data['category']}',
                `barcode` = '{$data['barcode']}',
                `sku` = '{$data['sku']}',
                `cost_price` = '{$data['cost_price']}',
                `price` = '{$data['price']}',
                `mod_id` = '{$data['goods_id']}'
              WHERE id={$data['id']} AND mod_id = {$data['goods_id']}";
      $result = mysqli_query($connect,$query);
      if(!$connect->error){
        return true;
      }else{
        echo json_encode(array('type'=>'error','msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' mods update model'));
        die;
      }
    }
    /*good update*/
    function good_update_model($data){
      $connect = $this->auxiliary->connect_user($this->token);
      if($data['image_goods'] != 'false'){
        $query="UPDATE `goods` set
                  `name` = '{$data['name']}',
                  `category_id` = '{$data['category']}',
                  `measure` = '{$data['measure']}',
                  `color` = '{$data['color']}',
                  `mod_id` = '{$data['mod_id']}',
                  `image` = '{$data['image_goods']}',
                  `cost_price` = '{$data['cost_price']}',
                  `price` = '{$data['price']}',
                  `barcode` = '{$data['barcode']}',
                  `sku` = '{$data['sku']}'
                WHERE id ={$data['goods_id']}";
      }else{
        $query="UPDATE `goods` set
                    `name` = '{$data['name']}',
                    `category_id` = '{$data['category']}',
                    `measure` = '{$data['measure']}',
                    `color` = '{$data['color']}',
                    `mod_id` = '{$data['mod_id']}',
                    `cost_price` = '{$data['cost_price']}',
                    `price` = '{$data['price']}',
                    `barcode` = '{$data['barcode']}',
                    `sku` = '{$data['sku']}'
                  WHERE id ={$data['goods_id']}";
      }
      $result = mysqli_query($connect,$query);
      if(!$connect->error){
        return true;
      }else{
        echo json_encode(array('type'=>'error','msg'=>$connect->error.''.$this->message['SYSTEM_ERROR_MESSAGE'].' good update model'));
        die;
      }
    }
  }
?>
