<?php
  header ('Access-Control-Allow-Origin: *');
  header ('Access-Control-Allow-Headers: Content-Type, X-Auth-Token, Origin, Authorization');
  include 'model.php';
  include '../../auxiliary_function.php';
  class Goods extends model{
    function __construct() {
      $this->auxiliary = new Auxiliary;
      $this->auxiliary->check_session();
      $this->token = JWT::decode($_POST['token'], 'kemeltek_kazakh_alshin');
      date_default_timezone_set($this->token->time_zone);
      $this->message = $this->auxiliary->lang_message();
    }
    /*category*/
    function category_add(){
      if(!isset($_POST['name_cat']) || $_POST['name_cat'] == ''){
        echo json_encode(array('type' => 'error','msg'=>$this->message['ENTER_NAME_CATEGORY_MESSAGE']));
        die;
      }else{
        if(!$this->auxiliary->check_length_input_var($_POST['name_cat'],45,3)){
          echo json_encode(array('type' => 'error','msg'=>$this->message['ENTER_NAME_CATEGORY_WORDS_MESSAGE']));
          die;
        }
      }
      if(!isset($_POST['parent_cat']) || $_POST['parent_cat'] == ''){
        echo json_encode(array('type' => 'error','msg'=>$this->message['SELECT_PARENT_CATEGORY_MESSAGE']));
        die;
      }
      if(!isset($_POST['color_cat']) || $_POST['color_cat'] == ''){
        $color = '#ffffff';
      }else{
        $color = $this->auxiliary->trimPostData($_POST['color_cat']);
      }
      $check_category_data = array(
        'name_cat'=>$_POST['name_cat'],
        'parent_cat'=>$_POST['parent_cat']
      );
      $check_category = model::check_category($check_category_data);
      if(!$check_category){
        echo json_encode(array('type'=> "error",'msg'=>$this->message['ERROR_CHECK_CATEGORY']));
        die;
      }

      if(!isset($_POST['image_cat']) || $_POST['image_cat'] == ''){
        $image_cat = '';
      }else{
        $directory = 'clients/'.$this->token->login.'/';
        $expfile = (explode(',',$_POST['image_cat']));
        $file_name = uniqid("file");
        $file_entry = $this->auxiliary->file_entry($expfile[1],$directory,$file_name.'.jpg');
        if($file_entry){
            $image_cat = 'http://files.kemeltek/'.$directory.$file_name.'.jpg';
        }else{
          echo json_encode(array('type'=> "error",'msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' file entry error'));
          die;
        }
      }
      $data = array(
          'name_cat'=>$this->auxiliary->trimPostData($_POST['name_cat']),
          'parent_cat'=>$this->auxiliary->trimPostData($_POST['parent_cat']),
          'color_cat'=>$color,
          'image'=>$image_cat,
          'status_delete'=>1,
          'status_show'=>1
      );
      $category_add = model::category_add_model($data);
      if($category_add){
        echo json_encode(array('type' => 'ok','msg'=>$this->message['SUCCESS_ADD_CATEGORY_MESSAGE']));
        die;
      }
    }
    function category_list(){
      $category_list = model::category_list_model();
      if(count($category_list) > 0){
        $cat_tree = $this->auxiliary->build_tree($category_list,"parent_category","key");
      }else{
        $cat_tree = $category_list;
      }

      echo json_encode(array('type'=>'ok','data'=>$cat_tree));
      die;
    }
    function category_update(){
      if(!isset($_POST['cat_id']) || $_POST['cat_id'] == ''){
        echo json_encode(array('type' => 'error','msg'=>$this->message['EMPTY_CATEGORY_STATUS_SHOW_ID']));
        die;
      }
      if(!isset($_POST['name_cat']) || $_POST['name_cat'] == ''){
        echo json_encode(array('type' => 'error','msg'=>$this->message['ENTER_NAME_CATEGORY_MESSAGE']));
        die;
      }else{
        if(!$this->auxiliary->check_length_input_var($_POST['name_cat'],45,3)){
          echo json_encode(array('type' => 'error','msg'=>$this->message['ENTER_NAME_CATEGORY_WORDS_MESSAGE']));
          die;
        }
      }
      if($_POST['parent_cat'] == $_POST['cat_id']){
        echo json_encode(array('type' => 'error','msg'=>$this->message['ERROR_UPDATE_CATEGORY_PARENT_CHANGE']));
        die;
      }
      if(!isset($_POST['parent_cat']) || $_POST['parent_cat'] == ''){
        echo json_encode(array('type' => 'error','msg'=>$this->message['SELECT_PARENT_CATEGORY_MESSAGE']));
        die;
      }
      if(!isset($_POST['color_cat']) || $_POST['color_cat'] == ''){
        $color = '#ffffff';
      }else{
        $color = $this->auxiliary->trimPostData($_POST['color_cat']);
      }

      if(!isset($_POST['image_cat']) || $_POST['image_cat'] == '' || $_POST['image_cat'] == 'false'){
        $image_cat = $_POST['image_cat'];

      }else{
        $directory = 'clients/'.$this->token->login.'/';
        $expfile = (explode(',',$_POST['image_cat']));
        $file_name = uniqid("file");
        $file_entry = $this->auxiliary->file_entry($expfile[1],$directory,$file_name.'.jpg');
        if($file_entry){
            $image_cat = 'http://files.kemeltek/'.$directory.$file_name.'.jpg';
        }else{
          echo json_encode(array('type'=> "error",'msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' file entry error'));
          die;
        }
      }
      $data = array(
          'cat_id'=>$this->auxiliary->trimPostData($_POST['cat_id']),
          'name_cat'=>$this->auxiliary->trimPostData($_POST['name_cat']),
          'parent_cat'=>$this->auxiliary->trimPostData($_POST['parent_cat']),
          'color_cat'=>$color,
          'image'=>$image_cat
      );
      $update_category = model::category_update_model($data);
      if($update_category){
        echo json_encode(array('type' => 'ok','msg'=>$this->message['SUCCESS_UPDATE_CATEGORY_MESSAGE']));
        die;
      }
    }
    function category_status_show(){
      if(!isset($_POST['cat_id']) || $_POST['cat_id'] == ''){
        echo json_encode(array('type' => 'error','msg'=>$this->message['EMPTY_CATEGORY_STATUS_SHOW_ID']));
        die;
      }
      if(!isset($_POST['status']) || $_POST['status'] == ''){
        echo json_encode(array('type' => 'error','msg'=>$this->message['EMPTY_CATEGORY_STATUS_SHOW_STATUS']));
        die;
      }
      $data = array(
        'cat_id' => $this->auxiliary->trimPostData($_POST['cat_id']),
        'status'=>$this->auxiliary->trimPostData($_POST['status'])
      );
      $update = model::category_status_show_model($data);
      if($update){
        echo json_encode(array('type'=>'ok','msg'=>$this->message['SUCCESS_STATUS_CATEGORY_EDIT_MESSAGE']));
        die;
      }
    }
    function category_status_delete(){
      if(!isset($_POST['cats']) || $_POST['cats'] == ''){
        echo json_encode(array('type' => 'error','msg'=>$this->message['EMPTY_CATEGORY_STATUS_SHOW_ID']));
        die;
      }
      $data = array(
        'cats' => $this->auxiliary->trimPostData($_POST['cats']),
      );
      $update = model::category_status_delete_model($data);
      if($update){
        echo json_encode(array('type'=>'ok','msg'=>$this->message['SUCCESS_DELETE_CATEGORY_MESSAGE']));
        die;
      }
    }
    function category_info(){
      if(!isset($_POST['cat_id']) || $_POST['cat_id'] == ''){
        echo json_encode(array('type' => 'error','msg'=>$this->message['EMPTY_CATEGORY_STATUS_SHOW_ID']));
        die;
      }
      $data = array(
        'id'=>$this->auxiliary->trimPostData($_POST['cat_id']),
      );
      $category_info = model::category_info_model($data);
      $category_list = model::category_list_filter_model($data);
      if(count($category_info)>0 ){
        if($category_list){
          $category_list_tree = $this->auxiliary->build_tree($category_list,"parent_category","key");
        }else{
          $category_list_tree= [];
        }
        echo json_encode(array('type' => 'ok','data'=>$category_info[0],'category_list'=>$category_list_tree));
        die;
      }else{
        echo json_encode(array('type'=>'error','msg'=>''.$this->message['EMPTY_CATEGORY_STATUS_SHOW_ID'].''));
        die;
      }
    }
    /*goods*/
    function checkMod($data){
      $newname = array();
      $newbarcode = array();
      $newsku = array();
      foreach ($data as $value) {
        if(isset($value['name']) && $value['name'] != ""){
            $name = $value['name'];
            if(!isset($newname[$name]))
            {
               $newname[$name] = $value['name'];
            }else{
              echo json_encode(array('type' => 'error','msg'=>'error mod name repeated'));
              die;
            }
        }else{
          echo json_encode(array('type' => 'error','msg'=>'error mod name undefined'));
          die;
        }
        if(isset($value['barcode']) && $value['barcode'] !=''){
          $barcode = $value['barcode'];
          if(!isset($newbarcode[$barcode]))
          {
            $newbarcode[$barcode] = $value['barcode'];
          }else{
            echo json_encode(array('type' => 'error','msg'=>'error mod barcode repeated'));
            die;
          }
        }
        if(isset($value['sku']) && $value['sku'] != ''){
          $sku = $value['sku'];
          if(!isset($newsku[$sku]))
          {
            $newsku[$sku] = $value['sku'];
          }else{
            echo json_encode(array('type' => 'error','msg'=>'error mod sku repeated'));
            die;
          }
        }
      }
      return true;
    }
    function goods_add(){
      //check name goods
      if(!isset($_POST['name']) || $_POST['name'] == ''){
        echo json_encode(array('type' => 'error','msg'=>$this->message['ENTER_NAME_GOODS_MESSAGE']));
        die;
      }else{
        if(!$this->auxiliary->check_length_input_var($_POST['name'],45,2)){
          echo json_encode(array('type' => 'error','msg'=>$this->message['ENTER_NAME_GOODS_WORDS_MESSAGE']));
          die;
        }
      }
      //check category goods
      if(!isset($_POST['category']) || $_POST['category'] == ''){
        echo json_encode(array('type' => 'error','msg'=>$this->message['SELECT_GOODS_CATEGORY_MESSAGE']));
        die;
      }
      //check mod type goods
      if(!isset($_POST['mod_type']) || $_POST['mod_type'] == ''){
        echo json_encode(array('type' => 'error','msg'=>"modification type request"));
        die;
      }
      //check measure dispatched
      if(!isset($_POST['measure'])){
        echo json_encode(array('type' => 'error','msg'=>"measure price not dispatched"));
        die;
      }
      //check color goods
      if(!isset($_POST['color']) || $_POST['color'] == ''){
        echo json_encode(array('type' => 'error','msg'=>"color request"));
        die;
      }
      //image check if true enter else empty
      if(!isset($_POST['image']) || $_POST['image'] == ''){
        $image = '';
      }else{
        $directory = 'clients/'.$this->token->login.'/';
        $expfile = (explode(',',$_POST['image']));
        $file_name = uniqid("file");
        $file_entry = $this->auxiliary->file_entry($expfile[1],$directory,$file_name.'.jpg');
        if($file_entry){
            $image = 'http://files.kemeltek/'.$directory.$file_name.'.jpg';
        }else{
          echo json_encode(array('type'=> "error",'msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' file entry error'));
          die;
        }
      }
      //check mode_type if 1 insert goods and mods else goods insert
      if($_POST['mod_type'] === '1'){
        if(!isset($_POST['goods']) || $_POST['goods'] == ''){
          echo json_encode(array('type' => 'error','msg'=>"goods list request"));
          die;
        }
        $mods = json_decode($_POST['goods'], true);
        $checkMod = $this->checkMod($mods);
        if($checkMod){
          $data = array(
              'name'=>$this->auxiliary->trimPostData($_POST['name']),
              'category'=>$this->auxiliary->trimPostData($_POST['category']),
              'measure'=>$this->auxiliary->trimPostData($_POST['measure']),
              'color'=>$this->auxiliary->trimPostData($_POST['color']),
              'mod_id'=>0,
              'barcode'=>NULL,
              'sku'=>NULL,
              'image_goods'=>$image
          );
          $good_insert = model::good_mod_insert_model($data);
          if($good_insert){
            foreach ($mods as $value) {
              $value['mod_id'] = $good_insert;
              $value['category'] = $data['category'];
              if(!isset($value['barcode']) || $value['barcode'] == ''){
                $value['barcode'] = NULL;
              }
              if(!isset($value['sku']) || $value['sku'] == ''){
                $value['sku'] = NULL;
              }
              model::mods_insert_model($value);
            }
            echo json_encode(array('type'=>'ok','msg'=>'success insert goods'));
            die;
          }
        }else{
          echo json_encode(array('type' => 'error','msg'=>'error mods undefined'));
          die;
        }
      }else{
        if(!isset($_POST['cost_price']) || $_POST['cost_price'] == ''){
          $cost_price = 0;
        }else{
          $cost_price = $this->auxiliary->trimPostData($_POST['cost_price']);
        }
        if(!isset($_POST['price']) || $_POST['price'] == ''){
          $price = 0;
        }else{
          $price = $this->auxiliary->trimPostData($_POST['price']);
        }
        if(!isset($_POST['barcode']) || $_POST['barcode'] == ''){
          $barcode = NULL;
        }else{
          $barcode = $this->auxiliary->trimPostData($_POST['barcode']);
        }
        if(!isset($_POST['sku']) || $_POST['sku'] == ''){
          $sku = NULL;
        }else{
          $sku = $this->auxiliary->trimPostData($_POST['sku']);
        }
        $data = array(
            'name'=>$this->auxiliary->trimPostData($_POST['name']),
            'category'=>$this->auxiliary->trimPostData($_POST['category']),
            'measure'=>$this->auxiliary->trimPostData($_POST['measure']),
            'color'=>$this->auxiliary->trimPostData($_POST['color']),
            'mod_id'=>0,
            'cost_price'=>$cost_price,
            'price'=>$price,
            'barcode'=>$barcode,
            'sku'=>$sku,
            'image_goods'=>$image
        );
        $good_insert = model::good_insert_model($data);
        echo json_encode(array('type'=>'ok','msg'=>$this->message['SUCCESS_INSERT_GOODS']));
        die;
      }
    }
    function goods_list(){
      $goods_list = model::goods_list_model();
      if(count($goods_list) > 0){
        $good_tree = $this->auxiliary->build_tree($goods_list,"mod_id","key");
      }else{
        $good_tree = $goods_list;
      }
      echo json_encode(array('type'=>'ok','data'=>$good_tree));
      die;
    }
    function good_info(){
      if(!isset($_POST['goods_id']) || $_POST['goods_id'] == ''){
        echo json_encode(array('type' => 'error','msg'=>$this->message['EMPTY_GOODS_ID']));
        die;
      }
      $data = array(
        'id'=>$this->auxiliary->trimPostData($_POST['goods_id']),
      );
      $goods = model::good_info_model($data);
      if($goods){
        $goods_tree = $this->auxiliary->build_tree($goods,"mod_id","key");
        echo json_encode(array('type' => 'ok','data'=>$goods_tree));
        die;
      }else{
        echo json_encode(array('type' => 'error','msg'=>$this->message['EMPTY_GOODS_ID']));
        die;
      }
    }
    function good_status_show(){
      if(!isset($_POST['good_id']) || $_POST['good_id'] == ''){
        echo json_encode(array('type' => 'error','msg'=>$this->message['EMPTY_CATEGORY_STATUS_SHOW_ID']));
        die;
      }
      if(!isset($_POST['status']) || $_POST['status'] == ''){
        echo json_encode(array('type' => 'error','msg'=>$this->message['EMPTY_GOODS_STATUS_SHOW_STATUS']));
        die;
      }
      $data = array(
        'good_id' => $this->auxiliary->trimPostData($_POST['good_id']),
        'status'=>$this->auxiliary->trimPostData($_POST['status'])
      );
      $update = model::goods_status_show_model($data);
      if($update){
        echo json_encode(array('type'=>'ok','msg'=>$this->message['SUCCESS_STATUS_GOODS_EDIT_MESSAGE']));
        die;
      }
    }
    function good_status_delete(){
      if(!isset($_POST['good_id']) || $_POST['good_id'] == ''){
        echo json_encode(array('type' => 'error','msg'=>$this->message['EMPTY_GOODS_ID']));
        die;
      }
      $data = array(
        'good_id' => $this->auxiliary->trimPostData($_POST['good_id']),
      );
      $update = model::goods_status_delete_model($data);
      if($update){
        echo json_encode(array('type'=>'ok','msg'=>$this->message['SUCCESS_DELETE_GOODS_MESSAGE']));
        die;
      }
    }
    function good_update(){
      if(!isset($_POST['goods_id']) || $_POST['goods_id'] == ''){
        echo json_encode(array('type' => 'error','msg'=>$this->message['EMPTY_GOODS_ID']));
        die;
      }
      if(!isset($_POST['category']) || $_POST['category'] == ''){
        echo json_encode(array('type' => 'error','msg'=>$this->message['SELECT_GOODS_CATEGORY_MESSAGE']));
        die;
      }
      if(!isset($_POST['name']) || $_POST['name'] == ''){
        echo json_encode(array('type' => 'error','msg'=>$this->message['ENTER_NAME_GOODS_MESSAGE']));
        die;
      }else{
        if(!$this->auxiliary->check_length_input_var($_POST['name'],45,2)){
          echo json_encode(array('type' => 'error','msg'=>$this->message['ENTER_NAME_GOODS_WORDS_MESSAGE']));
          die;
        }
      }
      if(!isset($_POST['mod_type']) || $_POST['mod_type'] == ''){
        echo json_encode(array('type' => 'error','msg'=>"modification type request"));
        die;
      }
      if(!isset($_POST['measure'])){
        echo json_encode(array('type' => 'error','msg'=>"measure price not dispatched"));
        die;
      }
      //check color goods
      if(!isset($_POST['color']) || $_POST['color'] == ''){
        echo json_encode(array('type' => 'error','msg'=>"color request"));
        die;
      }
      //image check if true enter else empty
      if(!isset($_POST['image']) || $_POST['image'] == '' || $_POST['image'] == 'false'){
        $image = '';
      }else{
        $directory = 'clients/'.$this->token->login.'/';
        $expfile = (explode(',',$_POST['image']));
        $file_name = uniqid("file");
        $file_entry = $this->auxiliary->file_entry($expfile[1],$directory,$file_name.'.jpg');
        if($file_entry){
            $image = 'http://files.kemeltek/'.$directory.$file_name.'.jpg';
        }else{
          echo json_encode(array('type'=> "error",'msg'=>''.$this->message['SYSTEM_ERROR_MESSAGE'].' file entry error'));
          die;
        }
      }

      if($_POST['mod_type'] === '1'){
        $mods = json_decode($_POST['goods'], true);
        $checkMod = $this->checkMod($mods);
        if($checkMod){
          $data = array(
              'goods_id'=>$this->auxiliary->trimPostData($_POST['goods_id']),
              'name'=>$this->auxiliary->trimPostData($_POST['name']),
              'category'=>$this->auxiliary->trimPostData($_POST['category']),
              'measure'=>$this->auxiliary->trimPostData($_POST['measure']),
              'color'=>$this->auxiliary->trimPostData($_POST['color']),
              'mod_id'=>0,
              'image_goods'=>$image
          );
          $good_insert = model::good_mod_update_model($data);
          if($good_insert){
            foreach ($mods as $value) {
              $value['mod_id'] = $good_insert;
              $value['goods_id'] = $data['goods_id'];
              $value['category'] = $data['category'];
              model::mods_update_model($value);
            }
            echo json_encode(array('type'=>'ok','msg'=>'success update goods'));
            die;
          }
        }else{
          echo json_encode(array('type' => 'error','msg'=>'error mods undefined'));
          die;
        }
      }else{
        if(!isset($_POST['cost_price'])){
          echo json_encode(array('type' => 'error','msg'=>"cost price not dispatched"));
          die;
        }
        if(!isset($_POST['price'])){
          echo json_encode(array('type' => 'error','msg'=>"price not dispatched"));
          die;
        }
        if(!isset($_POST['barcode'])){
          echo json_encode(array('type' => 'error','msg'=>"barcode not dispatched"));
          die;
        }
        if(!isset($_POST['sku'])){
          echo json_encode(array('type' => 'error','msg'=>"sku not dispatched"));
          die;
        }
        $data = array(
            'goods_id'=>$this->auxiliary->trimPostData($_POST['goods_id']),
            'name'=>$this->auxiliary->trimPostData($_POST['name']),
            'category'=>$this->auxiliary->trimPostData($_POST['category']),
            'measure'=>$this->auxiliary->trimPostData($_POST['measure']),
            'color'=>$this->auxiliary->trimPostData($_POST['color']),
            'mod_id'=>0,
            'cost_price'=>$this->auxiliary->trimPostData($_POST['cost_price']),
            'price'=>$this->auxiliary->trimPostData($_POST['price']),
            'barcode'=>$this->auxiliary->trimPostData($_POST['barcode']),
            'sku'=>$this->auxiliary->trimPostData($_POST['sku']),
            'image_goods'=>$image
        );
        $good_update = model::good_update_model($data);
        echo json_encode(array('type'=>'ok','msg'=>'success update goods'));
        die;
      }
    }
  }
  $Goods = new Goods;
  if(!empty($_POST['method'])){
      $get = $_POST['method'];
      if(method_exists($Goods,$get)){
          $Goods->$get();
      }else{
          echo json_encode(array('type' => 'error','sush'));
          die;
      }
  }else{
    echo json_encode(array('type' => 'error','get jok demek zapros bolgan jok'));
    die;      //$_POST['method'] jok demek zapros bolgan jok
  }
?>
