<?php
include 'jwt.php';
include 'auxiliary_model.php';
require_once 'smtp.php';
include 'config.php';
class Auxiliary extends Auxiliary_model
{
  function check_length_input_var($input,$max,$min){
    if((strlen($input) > $max) || (strlen($input) < $min)){
      return false;
    }else{
      return true;
    }
  }
  function trimPostData($data){
     $data = stripslashes($data);
     $data = htmlspecialchars($data);
     $data = trim($data);
     return $data;
  }
  function generate_code($data){
    $nowt = date("Y-m-d H:i:s");
    $code=$this->fish_password($nowt.$data);
    return $code;
  }
  function generate_password($length){
      $arr = array('a','b','c','d','e','f',
                       'g','h','i','j','k','l',
                       '1','2','3','G','H','I',
                       'J','K','L','8','9','0',
                       'm','n','o','p','r','s',
                       'M','N','O','P','R','S',
                       't','u','v','x','y','z',
                       'A','B','C','D','E','F',
                       'T','U','V','X','Y','Z',
                       '4','5','6','7');
      $pass = "";
      for($i = 0; $i < $length; $i++)
      {
        $index = rand(0, count($arr) - 1);
        $pass .= $arr[$index];
      }
      return $pass;
  }
  function sending_email($email,$titile,$message){
      $mailSMTP = new SendMailSmtpClass('ipamed.ipamed@yandex.kz', 'D6w1F7r8', 'ssl://smtp.yandex.ru', 'KEMELTEK.KZ', 465);
      $headers= "MIME-Version: 1.0\r\n";
      $headers .= "Content-type: text/html; charset=utf-8\r\n";
      $headers .= "From: kemeltek <ipamed.ipamed@yandex.kz>\r\n";
      $headers .= "To: kemeltek <".$email.">\r\n";
      $result =  $mailSMTP->send($email,$titile, $message, $headers);
      if($result === true){
         return true;
      }else{
	       return false;
      }
  }
  function fish_password($income_pwd){
      $FISH='jngjk345gjfdn2';
      return md5($FISH.md5($income_pwd));
  }
  function arrayResult($data){
       $result = array();
       while($row = mysqli_fetch_assoc($data)){
           $result[] = $row;
       }
       return $result;
   }
  function check_session(){
     if(!isset($_POST['token'])){
       echo json_encode(array('type'=>'error','msg'=>'Вы не авторизованы'));
       die;
     }else{

       if(!empty($_POST['token'])){
         $token = JWT::decode($this->trimPostData($_POST['token']), 'kemeltek_kazakh_alshin');
         if(empty($token->company_id) && empty($token->user_email)){
           echo json_encode(array('type'=>'error','msg'=>'Вы не авторизованы'));
           die;
         }
         return true;
       }else{
         echo json_encode(array('type'=>'error','msg'=>'Вы не авторизованы'));
         die;
       }
     }
  }
  function time_zone_user($token){
      $token = JWT::decode($token, 'kemeltek_kazakh_alshin');
      return $token->time_zone;
  }
  function lang_message(){
    if(!isset($_POST['lang'])){
      if(!isset($_POST['token'])){
        return parse_ini_file( 'lang/ru_RU_lang.ini' );
      }else{
        $token = JWT::decode($this->trimPostData($_POST['token']), 'kemeltek_kazakh_alshin');
        switch ($token->lang) {
          case 'ru':
            return parse_ini_file( 'lang/ru_RU_lang.ini' );
            break;
          case 'en':
            return parse_ini_file( 'lang/en_EN_lang.ini' );
            break;
          case 'kz':
            return parse_ini_file( 'lang/kz_KZ_lang.ini' );
            break;
          default:
            return parse_ini_file( 'lang/ru_RU_lang.ini' );
            break;
        }
      }
    }else{
      switch ($_POST['lang']) {
        case 'ru':
          return parse_ini_file( 'lang/ru_RU_lang.ini' );
          break;
        case 'en':
          return parse_ini_file( 'lang/en_EN_lang.ini' );
          break;
        case 'kz':
          return parse_ini_file( 'lang/kz_KZ_lang.ini' );
          break;
        default:
          return parse_ini_file( 'lang/ru_RU_lang.ini' );
          break;
      }
    }
  }
  function file_entry($file,$directory,$name){
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, 'http://files.kemeltek/input.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, "dirname=$directory&file=$file&fileName=$name");
    $resultCurl = curl_exec($curl);
    if($resultCurl == 1){
        return true;
    }else{
        return false;
    }
    curl_close($curl);
  }
  function build_tree($flat, $pidKey, $idKey = null){
      $grouped = array();
      foreach ($flat as $sub){
          $grouped[$sub[$pidKey]][] = $sub;
      }
      $fnBuilder = function($siblings) use (&$fnBuilder, $grouped, $idKey) {
          foreach ($siblings as $k => $sibling) {
              $id = $sibling[$idKey];
              if(isset($grouped[$id])) {
                  $sibling['children'] = $fnBuilder($grouped[$id]);
              }
              $siblings[$k] = $sibling;
          }

          return $siblings;
      };
      $tree = $fnBuilder($grouped[0]);
      return $tree;
  }
  function connect(){
      $connection = new mysqli(db_server, db_user, db_key, db_name);
      if (mysqli_connect_errno()) {
           mysqli_connect_error();
          die;
      }
      mysqli_set_charset($connection, 'utf8');
      return $connection;
  }
  function connect_user($token){
    $connection = new mysqli($token->db_host,$token->db_user,$token->db_pass,$token->db_name);
    if (mysqli_connect_errno()) {
         mysqli_connect_error();
        die;
    }
    mysqli_set_charset($connection, 'utf8');
    return $connection;
  }
}
?>
